package application;

import java.util.ArrayList;
import java.util.Random;

import javafx.scene.control.ListView;

public class DroneArena {
	
	private int xmax, ymax;
	private int posX, posY;
	public ArrayList<Drone> numDrone; // array for the drone that will contain all the drones
	//which are added by the user
	public int numDroneArena; // drone counter for GUI

	Random randomCoords;

	/**
	 * DroneArena constructor
	 * @param i
	 * @param j
	 */
	public DroneArena(int i, int j) {
		
		xmax = i;
		ymax = j;
		randomCoords = new Random();
		numDrone = new ArrayList<Drone>();
	}
	/**
	 * this function will add the drones in the arena when the arena is created.
	 * drones will be added to the ListView method too
	 * this method will also add the aerials which is inherited from the drone class
	 * GUI will continue to add drones and aerials until there is no more space within the canvas
	 * @param mc
	 * @param Vehicles
	 * @param type
	 */
	
	public void addDrone(MyCanvas mc, ListView<Drone> Vehicles, char type) {
		boolean decider = true;
		while(decider) {
			posX = randomCoords.nextInt(xmax);
			posY = randomCoords.nextInt(ymax);
			
			if(posX > 0 && posX < xmax -20 && posY > 0 && posY < ymax -25 && !isHere(posX, posY)) {
				Direction.selectDirection x = Direction.selectDirection.getRandomDirection();
				if (type == 'd') {
					numDrone.add(new Drone(posX, posY, x));	
				} else if (type == 'a') {
					numDrone.add(new Aerial(posX, posY, x));	
				}
				numDroneArena++;
				decider = false;
			}
			mc.createDrone(this);
			DroneInterface.listDrones(Vehicles);
			
		}
	}
	
	/**
	 * this function will move all the drones
	 * @param mc
	 */
	
	public void moveAllDrones(MyCanvas mc) {
		for (Drone d: numDrone) {
			d.moveDrone(this);
		}
		mc.changeCanvas(this);
	}
	
	/**
	 * checks if the drones can move to given coordinates, it also checks if the drone position might be
	 * out of arena size.
	 */
	
	public boolean canMoveHere(double x, double y) { 
		return !isHere(x,y) && x > 0 && y > 0 && x< xmax -20 && y < ymax - 25;
		
		/*boolean move = false;
		move = !isHere(x,y) && x > 0 && y > 0 && x< xmax -20 && y < ymax - 25;
		for (Drone d: numDrone);
			
			if (d.isHere = moveDrone) {
				canMoveHere = false;
		return move;
			
			}*/	
		
	}
	
	
	/**
	 * This function will compare parameters with existing drone positions and checks if drone holds
	 * that position. If the drone collide with the wall or another drone, then it will change 
	 * its direction.
	 * @param lengthWise
	 * @param crossWise
	 * @return
	 */
	public boolean isHere(double lengthWise, double crossWise) {
		int a =0,  b = 0;
		
		/**
		 for (Drone d: numDrone) {
			if(x < d.x + d.width && x + Dx.width > d.x
				&& y < d.y + d.height && y + Dx.height > d.y
				)
		 
		}*/
		
		for (Drone d: numDrone) {
			if (crossWise == d.getY() || crossWise + 30 == d.getY() || crossWise -30 == d.getY()) {
				++a;
			}
			if (lengthWise == d.getX() || lengthWise + 30 == d.getX() || lengthWise -30 == d.getX()) {
				++b;
			} else {
				continue;
			}
		}
		if ( a> 0 && b >0) {
			System.out.println("Route Blocked!");
			return true;
		} else {
			return false;
		}
		
		
	}
	
	/**
	 * This function will display the information about arena and number of drones.
	 */
	
	public String toString() {
		return "The size of the arena is: " + xmax + " * " + ymax + ".\n" + "Drone count: " + numDroneArena + ".";
	}

	
}
	
	
	