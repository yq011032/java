package application;



import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Simple program to show arena with multiple drones
 * @author shsmchlr
 *
 */
public class DroneInterface extends Application {
	private int canvasWidth = 900, canvasHeight = 600;
	private MyCanvas mc;
	private static DroneArena Arena;
	private static AnimationTimer time;
	private static Drone d;
	
	private void showMessage(String TStr, String CStr) {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle(TStr);
		alert.setHeaderText(null);
		alert.setContentText(CStr);
		alert.showAndWait();	
	}
	private void viewAbout() {
		showMessage( "About",  " Drone Simulate GUI Version" );
	}
	
	private void viewHelp() {
		showMessage( "Help",  " Please ADD drone and aeriaL and then press the START button to run the program" );
	}
	
	
	private void viewOpen() {
		showMessage( "Open", "Drone Simulator" );
	}

	/**
 * function to  the menu
 */
	public MenuBar setMenu() {
		MenuBar menuBar = new MenuBar(); // create menu
		Menu mInfo = new Menu("Info");  // creating "Info" menu
		
		
		MenuItem mAbout = new MenuItem ("About"); // creating sub menu "About"
		mAbout.setOnAction(new EventHandler<ActionEvent>() { // setting action to perform
			
			@Override
			public void handle(ActionEvent actionEvent) {
				viewAbout(); // show the About message
			}
		});
		
		
		
		MenuItem mHelp = new MenuItem ( "Help");  //creating sub menu "About"
		mHelp.setOnAction(new EventHandler<ActionEvent>(){ // setting action to perform
			public void handle(ActionEvent actionEvent) {
				viewHelp(); // show the Help message
	}
		});
		
		mInfo.getItems().addAll(mAbout, mHelp);  // adding sub menus "About" and "Help" to the menu "Info"
		
		Menu mFile = new Menu("File");  // creating File menu
		MenuItem mExit = new MenuItem("Exit"); // creating exit sub menu 
		mExit.setOnAction(new EventHandler<ActionEvent>(){ // set action to perform
			public void handle(ActionEvent t) { // add handler
				System.exit(0); //exit program
	}
		});
		mFile.getItems().addAll(mExit); // adding Exit sub menu to the File menu
		
		
		  Menu mEdit = new Menu("Edit"); // create Edit menu
		  MenuItem mOpen = new MenuItem("Open"); // create open sub menu
		  mOpen.setOnAction(new EventHandler<ActionEvent>(){ // set action to perform
			public void handle(ActionEvent actionEvent) { //add handler
				viewOpen(); // show the open message
				
				}
				});
		
		 mEdit.getItems().addAll(mOpen); // adding Open sub menu to the Edit menu
		 
	
		menuBar.getMenus().addAll(mFile, mEdit, mInfo); // adding File, Edit, and Info menus
		return menuBar; // return the menu, menu would be added
	}
	
	/**
	 * function to add the drones to the list and display drones' details on their position 
	 * when placed
	 * @param DroneGroup
	 */
	
	
	public static void listDrones(ListView<Drone> DroneGroup) {
		DroneGroup.getItems().clear(); // clearing the list view 
		for (Drone d: Arena.numDrone) { // checking the drones in the list 
			DroneGroup.getItems().add(d); // adding drones to the list view
		}
	}
	
	
	/**
	 * creating the stage for the application, the features will be shown 
	 * on the window when the program is executed
	 */

	@Override
	public void start(Stage stagePrimary) throws Exception {
		stagePrimary.setTitle(" 30011032");
		Text Title = new Text(" The Field");
		Title.setX(375);
		Title.setY(-10);
		Title.setFont(Font.font( "arial", FontWeight.BOLD, FontPosture.ITALIC, 27));
		Title.setFill(Color.BLUE);
		Title.setStrokeWidth(1.25);
		Title.setStroke(Color.DARKGREEN);
		
		Group root = new Group(Title); 
		Canvas canvas = new Canvas(890, 490); //setting canvas details 
		root.getChildren().add(canvas); //and adding it to the stage, with the title
		
		
		Image image = new Image("C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\Java\\moon2.png");
		ImageView imageView = new ImageView(image);
		imageView.setFitHeight(40);
		imageView.setFitWidth(40);
		imageView.setX(600);
		imageView.setY(35);
		
		
		Image cloud = new Image("C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\Java\\newCloud.png");
		ImageView subImage = new ImageView(cloud);
		subImage.setFitHeight(50);
		subImage.setFitWidth(50);
		subImage.setX(200);
		subImage.setY(35);
		
		
		root.getChildren().add(imageView);
		root.getChildren().add(subImage);
		
		
		
	
		mc = new MyCanvas(canvas.getGraphicsContext2D(), canvasWidth, canvasHeight);
		// create canvas
		Arena = new DroneArena(890, 490); // creating drone arena using x and y and adding it
		mc.setFillArenaColour(canvasWidth, canvasHeight); //setting the colour
		ListView<Drone> Vehicles = new ListView<>(); // creating list view for drones' details to be shown
		
		
		/**
		 * setting timer which is used to start and stop 
		 * the movements of all drones
		 */
		time = new AnimationTimer() { 
			public void handle(long now) {
				Arena.moveAllDrones(mc);
				listDrones(Vehicles);
			}
		};
		
		Vehicles.setStyle(
				//"-fx-border-color: #d98f18;" +
				"-fx-border-width: 10px;");
		
		/**
		 * adding title and text formatting for title
		 */
		Text text = new Text(" Operative Drones: ");
		text.setFont(Font.font( "arial", FontWeight.BOLD , FontPosture.ITALIC, 25));
		text.setFill(Color.BLUE);// 
		text.setStrokeWidth(1.25); 
		text.setStroke(Color.BROWN);
		
		VBox vbList = new VBox(text);
		vbList.getChildren().addAll(Vehicles);
		vbList.setAlignment(Pos.CENTER);
		//vbList.setPadding(new Insets(0, 0, 0, 50 ));
		vbList.setPadding(new Insets(0, 30, 20, 50 ));
		
		
		/**
		 * adding image and setting its size
		 */
		Image addr = new Image("C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\new images drone gui\\plus.png");
		ImageView viewInsert = new ImageView(addr);
		viewInsert.setFitHeight(40);
		viewInsert.setFitWidth(40);
		
		/**
		 * formatting the button for adding drone
		 */
		Button insertDroneButton = new Button("ADD DRONE", viewInsert);
        insertDroneButton.setStyle(
				"-fx-border-color: #000dff;" +
				"-fx-border-width: 3px;" +
				"-fx-font-size: 1.5em;" +
				"-fx-text-fill: #000dff;" +
				"-fx-background-color: #ffffff;");
		
		insertDroneButton.setOnAction(e -> {
			Arena.addDrone(mc, Vehicles, 'd');
		});
		
		insertDroneButton.setMinSize( 200, 50); // specifying the button size
		insertDroneButton.setMaxSize( 125, 50); // specifying the button size
		
	
		
		Image aerial = new Image( "C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\new images drone gui\\plus.png");
		ImageView viewAerial = new ImageView(aerial);
		viewAerial.setFitHeight(40);
		viewAerial.setFitWidth(40);
		 
		
		
		Button insertAerialButton = new Button("ADD AERIAL", viewAerial);
		insertAerialButton.setStyle(
				"-fx-border-color: #000dff;" +
				"-fx-border-width: 3px;" +
				"-fx-font-size: 1.5em;" +
				"-fx-text-fill: #000dff;" +
				"-fx-background-color: #ffffff;");
		
		insertAerialButton.setOnAction(e -> {
			Arena.addDrone(mc, Vehicles, 'a');
		});
		
		insertAerialButton.setMinSize( 200, 50);
		insertAerialButton.setMaxSize( 125, 50);
		

		
		Image start = new Image( "C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\new images drone gui\\play.png");
		ImageView viewStart = new ImageView(start);
		viewStart.setFitHeight(40);
		viewStart.setFitWidth(40);
		
	   
	    Button MoveDronesButton = new Button( "START", viewStart); //start button to start the animation
		MoveDronesButton.setStyle(
				"-fx-border-color: #15c218;" +
						"-fx-border-width: 3px;" +
						"-fx-font-size: 1.5em;" +
						"-fx-text-fill: #000523;" +
						"-fx-background-color: #ffffff;");
		
		MoveDronesButton.setOnAction(e -> time.start()); // starts timer and and calls the moveAllDrones method
		MoveDronesButton.setMinSize( 125, 50);
		MoveDronesButton.setMaxSize( 125, 50);
		
		
		Image stop = new Image( "C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\new images drone gui\\pause.png");
		ImageView viewStop = new ImageView(stop);
		viewStop.setFitHeight(40);
		viewStop.setFitWidth(40);
		
		
		Button DroneStopMove = new Button( "PAUSE", viewStop); // Pause button the stop the animation
		DroneStopMove.setStyle(
				"-fx-border-color: #ff0000;" +
						"-fx-border-width: 3px;" +
						"-fx-font-size: 1.4em;" +
						"-fx-text-fill: #ff0000;" +
						"-fx-background-color: #ffffff;");
		
		DroneStopMove.setOnAction(e -> time.stop()); //stops timer and calls the moveAllDrones method 
		
		DroneStopMove.setMinSize( 125, 50);
		DroneStopMove.setMaxSize( 125, 50);
				
		Image arrow = new Image( "C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\new images drone gui\\arrow.png");
		ImageView viewArrow = new ImageView(arrow);
		viewArrow.setFitHeight(34);
		viewArrow.setFitWidth(34);

		
		
		Button ArenaDim = new Button( "ARENA STATS", viewArrow);
		ArenaDim.setStyle(
				"-fx-border-color: #d98f18;" +
						"-fx-border-width: 3px;" +
						"-fx-font-size: 1.5em;" +
						"-fx-text-fill: #d98f18;" +
						"-fx-background-color: #ffffff;");
				
				
		ArenaDim.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent actionEvent) {
				showMessage( "Arena Dimensions", Arena.toString());	
			}
		});
		
		
	 Image info = new Image( "C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\new images drone gui\\information.png");
		ImageView viewInfo = new ImageView(info);
		viewInfo.setFitHeight(34);
		viewInfo.setFitWidth(34);

		
		
		Button ArenaInfo = new Button( "INFORMATION", viewInfo);
		ArenaInfo.setStyle(
				"-fx-border-color: #000ccc;" +
						"-fx-border-width: 3px;" +
						"-fx-font-size: 1.5em;" +
						"-fx-text-fill: #000ddd;" +
						"-fx-background-color: #ffffff;");
				
				
		ArenaInfo.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent actionEvent) {
				showMessage( "Information", " This is an example of GUI in Java Programming Language");	
			}
		});

		/**
		 * sets padding and formatting for console
		 */
		
		HBox hbButtons = new HBox(20);
		//hbButtons.setAlignment(Pos.CENTER_RIGHT);
		hbButtons.setAlignment(Pos.CENTER_LEFT);
		//hbButtons.setPadding(new Insets(0, 160, 50 ,0));
		hbButtons.setPadding(new Insets(0, 160, 50 ,90));

		hbButtons.getChildren().addAll(insertDroneButton, insertAerialButton,MoveDronesButton, 
				DroneStopMove, ArenaDim, ArenaInfo); // adding all the buttons to the console
		
		BorderPane bp = new BorderPane(); // creating the border pane 
		
		/**
		 * formatting the border pane
		 */
		bp.setTop(setMenu());
		bp.setCenter(root);
		bp.setBottom(hbButtons);
		bp.setRight(vbList);
		
		
		Scene scene = new Scene(bp, 1400, 700); // setting the scene with border pane
		stagePrimary.setScene(scene);
		stagePrimary.show();
		
	}
	/**
	 * main method to launch the application
	 * @param args
	 */
	
	public static void main(String[] args) {
		Application.launch(args);
	}

}
