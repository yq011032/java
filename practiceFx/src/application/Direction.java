package application;

import java.util.Random;

public class Direction {

public enum selectDirection{
	North,
	NorthNorthEast,
	NorthEast,
	EastNorthEast,
	NorthNorthWest,
	NorthWest,
	WestNorthWest,
	South,
	EastSouthEast,
	SouthEast,
	SouthSouthEast,
	WestSouthWest,
	SouthWest,
	SouthSouthWest,
	East,
	West;
	
	
	/**
	 * This function will select the next random direction from selectDirection.
	 * @return
	 */

	public static selectDirection getRandomDirection(){
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
		
	/**
	 * This function will check if the drone's direction is possible and if it isn't, the drone will change 
	 * to the next suitable direction. 
	 * @param n
	 * @return
	 */
	
	public selectDirection getNextDirection(selectDirection n){
		if ( n == selectDirection.North) {
			return selectDirection.NorthNorthEast;
		}
		
		if ( n == selectDirection.NorthNorthEast) {
			return selectDirection.NorthEast;
		}
		if ( n == selectDirection.NorthEast) {
			return selectDirection.EastNorthEast;
		}
		
		if ( n == selectDirection.WestNorthWest) {
			return selectDirection.NorthWest;
		}
		
		if ( n == selectDirection.NorthWest) {
			return selectDirection.NorthNorthWest;
			
		}
		
		if ( n == selectDirection.NorthNorthWest) {
			return selectDirection.North;
		}
		
		if ( n == selectDirection.EastNorthEast) {
			return selectDirection.East;
		}
		if ( n == selectDirection.East) {
			return selectDirection.EastSouthEast;
		}
		
		if ( n == selectDirection.EastSouthEast) {
			return selectDirection.SouthEast;
		}
		if ( n == selectDirection.South) {
			return selectDirection.SouthSouthWest;
		}
		if ( n == selectDirection.SouthEast) {
			return selectDirection.SouthSouthEast;
		}
		if ( n == selectDirection.SouthSouthEast) {
			return selectDirection.South;
		}
		if ( n == selectDirection.SouthSouthWest) {
			return selectDirection.SouthWest;
		}
		if ( n == selectDirection.SouthWest) {
			return selectDirection.WestSouthWest;
		}
		if ( n == selectDirection.WestSouthWest) {
			return selectDirection.West;
		}
		if ( n == selectDirection.West) {
			return selectDirection.WestNorthWest;
		} 
		return selectDirection.North;		
	 }
		 }
}