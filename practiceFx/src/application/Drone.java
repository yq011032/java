package application;

import javafx.scene.image.Image;

public class Drone {
	
	double x;

	double y;

	protected int DroneID, dx, dy;
    protected char type;
	private char z;
	Direction.selectDirection facing;
	
	private static int DroneCount = 0;
	
	/**
	 * function to create drone constructor
	 * @param Dx
	 * @param Dy
	 * @param f
	 */
	public Drone (int Dx, int Dy, Direction.selectDirection f) {
		x = Dx;
		y = Dy;
	    DroneID = DroneCount++;
	    dx = 1;
	    dy = 1;
	    facing = f;
	    type = 'd';
	   // droneVehicle = new Image("C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\art.png");
	}
		
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public void setXY(int nx, int ny) {
		x = nx;
		y = ny;
		
	}
	
	/**
	 * this function display the information on the drone
	 */
	public String toString() {
		return "Drone " + DroneID + " at " + x + ", " + y + " facing " + facing;
	
	}
/**
 * This function will allow the drones to move in the specified directions if the space is not 
 * occupied by another drone or a wall, depends on the direction specified on a 16-point compass.
 * If the drone can't move, then it will change its direction and attempts again.
 * @param DA
 */
	public void moveDrone(DroneArena DA) {
		double horizontal = 0, vertical = 0;
		if (facing == Direction.selectDirection.North) {
			horizontal = x;
			vertical = y+1;
		} else if (facing == Direction.selectDirection.NorthNorthEast) {
			horizontal = x + 0.5;
			vertical = y +1;
		} else if (facing == Direction.selectDirection.EastNorthEast) {
			horizontal = x + 1;
			vertical = y +0.5;
		} else if (facing == Direction.selectDirection.NorthEast) {
			horizontal = x + 1;
			vertical = y +1;
		}else if (facing == Direction.selectDirection.NorthNorthWest) {
			horizontal = x + 0.5;
			vertical = y +1;
		}else if (facing == Direction.selectDirection.WestNorthWest) {
			horizontal = x -1;
			vertical = y +0.5;
		}else if (facing == Direction.selectDirection.NorthWest) {
			horizontal = x + 1;
			vertical = y -1;
		}else if (facing == Direction.selectDirection.East) {
			horizontal = x + 1;
			vertical = y;
		}else if (facing == Direction.selectDirection.EastSouthEast) {
			horizontal = x + 1;
			vertical = y -0.5;
		}else if (facing == Direction.selectDirection.SouthSouthEast) {
			horizontal = x + 0.5;
			vertical = y -1;
		}else if (facing == Direction.selectDirection.South) {
			horizontal = x;
			vertical = y -1;
		}else if (facing == Direction.selectDirection.SouthEast) {
			horizontal = x -1;
			vertical = y +1;
		}else if (facing == Direction.selectDirection.WestSouthWest) {
			horizontal = x -1;
			vertical = y -0.5;
		}else if (facing == Direction.selectDirection.SouthSouthWest) {
			horizontal = x -0.5;
			vertical = y -1;
		}else if (facing == Direction.selectDirection.SouthWest) {
			horizontal = x -1;
			vertical = y -1;
		}else if (facing == Direction.selectDirection.West) {
			horizontal = x -1;
			vertical = y;
		}
		
		boolean go = DA.canMoveHere(horizontal, vertical);
		if (go) {
			this.x = horizontal;
			this.y = vertical;
			System.out.println(this.toString());
		} else {
			facing = facing.getNextDirection(facing);
		}
		
	}
	
}
	
	
	
	