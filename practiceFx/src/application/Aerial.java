package application;

public class Aerial extends Drone {
	
	private int Dx, Dy;
	private  Direction.selectDirection route;
	
	
	/**
	 * constructor for Aerial
	 * @param Dx
	 * @param Dy
	 * @param route
	 */
	Aerial(int Dx, int Dy, Direction.selectDirection route) {
		super(Dx, Dy, route);
		type = 'a';
		
	}
	
	/**
	 * This function will display the information about aerials.
	 */

	public String toString() {
		return "Aerial " + DroneID + " at " + x + ", " + y + " facing " + facing;
	
	}
}
