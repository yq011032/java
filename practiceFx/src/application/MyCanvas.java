package application;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MyCanvas extends Application {
	
	int canvasWidthSize;
	int canvasHeightSize;
	
	Image droneVehicle = new Image("C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\art.png");
	Image aerialVehicle = new Image("C:\\Users\\abdul\\OneDrive\\Documents\\year 2 computeer science\\Java\\new.gif");
	GraphicsContext gc;
	
	/**
	 * constructor for MyCanvas
	 * @param g
	 * @param xcs
	 * @param ycs
	 */
	public MyCanvas(GraphicsContext g, int xcs, int ycs) {
		gc = g;
		canvasWidthSize = xcs;
		canvasHeightSize = ycs;
	}
	
	/**
	 * This function will fill canvas and its border with the colours
	 * @param CanvasWidth
	 * @param CanvasHeight
	 */
	
	public void setFillArenaColour(int CanvasWidth, int CanvasHeight) {
		gc.setFill(Color.GRAY);
		gc.fillRect(0, 0, CanvasWidth, CanvasHeight);
		gc.setStroke(Color.CYAN);
		gc.strokeRect(0, 0, CanvasWidth, CanvasHeight);
		
	}
	
	/**
	 * This function will change the canvas each time which drones move.
	 * @param Arena
	 */
	public void changeCanvas(DroneArena Arena) {
		gc.clearRect(0, 0, canvasWidthSize, canvasHeightSize);
		setFillArenaColour(canvasWidthSize, canvasHeightSize);
		createDrone(Arena);
	}
	
	
	/**
	 * This function will draw the drone image onto the canvas
	 * @param Arena
	 */
	public void createDrone(DroneArena Arena) {
		for ( Drone d: Arena.numDrone) {
			if (d.type == 'd') {
				gc.drawImage(droneVehicle, d.getX(), d.getY(), 25, 25);				
			} else if (d.type == 'a') {
				gc.drawImage(aerialVehicle, d.getX(), d.getY(), 25, 25);
			}
		}
	}
	
		 
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
